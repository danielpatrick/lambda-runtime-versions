exports.handler = async (_event, _context) => {
  return {
    statusCode: 200,
    body: process.version.replace(/^v/, ''),
  }
}
