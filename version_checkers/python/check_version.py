import sys


def handler(_event, _context):
    return {
        "statusCode": 200,
        "body": ".".join(str(n) for n in sys.version_info[:3]),
    }
