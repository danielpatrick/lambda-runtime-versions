AWSTemplateFormatVersion: "2010-09-09"
Description: APIs for finding out the current versions of lambda runtimes
Transform: AWS::Serverless-2016-10-31

Globals:
  Function:
    Timeout: 29
    MemorySize: 128
    Runtime: python3.12
    CodeUri: ./lambda_runtime_versions
    Environment:
      Variables:
        SERVICE_NAME: !Ref AWS::StackName
        ENV: !Ref Env

Parameters:
  Env:
    Type: String
    Description: "Env name - dev, staging or prod"
    AllowedValues:
      - dev
      - staging
      - prod
    Default: dev
  DomainCertArn:
    Type: String
    Description: "Certificate arn for the domain we're going to create"
  CustomDomainName:
    Type: String
    Description: "The domain name to create, eg api.example.com"
    Default: "NOT-SUPPLIED"
  IsDefaultBranch:
    Type: String
    Description: "'true' if branch is default else 'false'"

Conditions:
  DefaultBranch: !Equals [ !Ref IsDefaultBranch, "true" ]

Resources:
  CustomDomain:
    Type: AWS::ApiGatewayV2::DomainName
    Condition: DefaultBranch
    Properties:
      DomainName: !Ref CustomDomainName
      DomainNameConfigurations:
        - EndpointType: REGIONAL
          CertificateArn: !Ref DomainCertArn
          SecurityPolicy: TLS_1_2

  ApiMapping:
    Type: AWS::ApiGatewayV2::ApiMapping
    Condition: DefaultBranch
    Properties:
      ApiId: !Ref ServerlessHttpApi
      DomainName: !Ref CustomDomainName
      Stage: !Ref ServerlessHttpApiApiGatewayDefaultStage

  FeedApiHandler:
    Type: AWS::Serverless::Function
    Properties:
      MemorySize: 512
      Handler: application.feed_api.handler
      Events:
        All:
          Type: HttpApi
      Policies:
        - DynamoDBReadPolicy:
            TableName: !Ref VersionFeedTable
      Environment:
        Variables:
          SOURCE_NAME: FeedApiHandler
          FEED_TABLE_NAME: !Ref VersionFeedTable

  UpdateVersionsHandler:
    Type: AWS::Serverless::Function
    Properties:
      MemorySize: 512
      Handler: application.update_versions.handler
      Events:
        Schedule:
          Type: Schedule
          Properties:
            Schedule: "cron(0 * * * ? *)"
            Enabled: !Ref DefaultBranch
      Policies:
        - DynamoDBWritePolicy:
            TableName: !Ref LatestVersionTable
      Environment:
        Variables:
          SOURCE_NAME: UpdateVersionsHandler
          LATEST_TABLE_NAME: !Ref LatestVersionTable
          DOMAIN_NAME: !Ref CustomDomainName

  VersionStreamHandler:
    Type: AWS::Serverless::Function
    Properties:
      MemorySize: 512
      Handler: application.version_stream.handler
      Events:
        Dynamodb:
          Type: DynamoDB
          Properties:
            Stream: !GetAtt LatestVersionTable.StreamArn
            StartingPosition: TRIM_HORIZON
            BatchSize: 1
      Policies:
        - DynamoDBWritePolicy:
            TableName: !Ref VersionFeedTable
      Environment:
        Variables:
          SOURCE_NAME: VersionStreamHandler
          FEED_TABLE_NAME: !Ref VersionFeedTable

  LatestVersionTable:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: "Runtime"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "Runtime"
          KeyType: "HASH"
      StreamSpecification:
        StreamViewType: NEW_AND_OLD_IMAGES
      BillingMode: PAY_PER_REQUEST

  VersionFeedTable:
    Type: AWS::DynamoDB::Table
    Properties:
      AttributeDefinitions:
        - AttributeName: "Language"
          AttributeType: "S"
        - AttributeName: "Version"
          AttributeType: "S"
        - AttributeName: "LSI2SK"
          AttributeType: "S"
        - AttributeName: "GSI1PK"
          AttributeType: "S"
        - AttributeName: "Time"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "Language"
          KeyType: "HASH"
        - AttributeName: "Version"
          KeyType: "RANGE"
      LocalSecondaryIndexes:
        - IndexName: "LSI1"
          KeySchema:
          - AttributeName: "Language"
            KeyType: "HASH"
          - AttributeName: "Time"
            KeyType: "RANGE"
          Projection:
            ProjectionType: "ALL"
        - IndexName: "LSI2"
          KeySchema:
          - AttributeName: "Language"
            KeyType: "HASH"
          - AttributeName: "LSI2SK"
            KeyType: "RANGE"
          Projection:
            ProjectionType: "ALL"
      GlobalSecondaryIndexes:
        - IndexName: "GSI1"
          KeySchema:
            - AttributeName: "GSI1PK"
              KeyType: "HASH"
            - AttributeName: "Time"
              KeyType: "RANGE"
          Projection:
            ProjectionType: "ALL"
      BillingMode: PAY_PER_REQUEST

# Version check lambdas

  Python39:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: python3.9
      CodeUri: ./version_checkers/python
      Handler: check_version.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /python/3.9
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Python39

  Python310:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: python3.10
      CodeUri: ./version_checkers/python
      Handler: check_version.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /python/3.10
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Python310

  Python311:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: python3.11
      CodeUri: ./version_checkers/python
      Handler: check_version.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /python/3.11
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Python311

  Python312:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: python3.12
      CodeUri: ./version_checkers/python
      Handler: check_version.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /python/3.12
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Python312

  Node16:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: nodejs16.x
      CodeUri: ./version_checkers/node
      Handler: checkVersion.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /node/16
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Node16

  Node18:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: nodejs18.x
      CodeUri: ./version_checkers/node
      Handler: checkVersion.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /node/18
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Node18

  Node20:
    Type: AWS::Serverless::Function
    Properties:
      Runtime: nodejs20.x
      CodeUri: ./version_checkers/node
      Handler: checkVersion.handler
      Events:
        Api:
          Type: HttpApi
          Properties:
            Path: /node/20
            Method: get
      Environment:
        Variables:
          SOURCE_NAME: Node20

# End version check lambdas

Outputs:
  LatestVersionTableName:
    Description: The name of the latest version table
    Value: !Ref LatestVersionTable

  VersionFeedTableName:
    Description: The name of the feed table
    Value: !Ref VersionFeedTable

  BaseUrl:
    Description: API URL - using ApiGatewayV2 URL if custom domain is not deployed
    Value: !If
      - DefaultBranch
      - CustomDomainName
      - !Sub "${ServerlessHttpApi}.execute-api.${AWS::Region}.amazonaws.com"
