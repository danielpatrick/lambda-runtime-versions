import json
from base64 import b64encode
from unittest.mock import MagicMock, patch

import pytest

from infrastructure.handlers.api_gateway.exceptions import HttpErrorBase
from infrastructure.handlers.api_gateway.handler import ApiGatewayV2Handler
from unit_tests.factories import api_event


class TestApiGatewayV2Handler:
    @pytest.fixture
    def event(self):
        yield api_event("post", "/")

    @pytest.fixture(autouse=True)
    def logger(self):
        yield MagicMock()

    @pytest.fixture
    def PatchedHandler(self, logger):
        class Handler(ApiGatewayV2Handler):
            SERVICE = "test-service"
            SOURCE = "test-source"

            def create_logger(self):
                return logger

        yield Handler

    @pytest.fixture
    def ReturnEventHandler(self, PatchedHandler):
        class Handler(PatchedHandler):
            @property
            def response_headers(self):
                headers = {
                    k: v for k, v in self.headers.items() if isinstance(v, str)
                }
                multi_value_headers = {
                    k: v
                    for k, v in self.headers.items()
                    if isinstance(v, list)
                }

                return {
                    "headers": headers,
                    "multi_value_headers": multi_value_headers,
                }

            def execute(self):
                return self.create_response(
                    **self.response_headers,
                    body=self.body,
                )

        yield Handler

    @pytest.fixture
    def ReturnEventWith201Handler(self, ReturnEventHandler):
        class Handler(ReturnEventHandler):
            def execute(self):
                body = self.body
                is_base64 = self.event["isBase64Encoded"]

                if is_base64:
                    body = b64encode(body.encode("utf-8")).decode("utf-8")

                return self.create_response(
                    **self.response_headers,
                    status_code=201,
                    body=body,
                    is_base64=is_base64,
                )

        yield Handler

    def test_can_decode_base64_iso_8859_1_body_if_text_header(
        self, ReturnEventHandler
    ):
        # This test confirms that we're using the get_encoding_from_headers
        # util from the requests library to decide on default encodings for
        # certain content types (eg text and json)
        event = api_event(
            "post",
            "/",
            body='{"currencySymbol": "£"}'.encode("iso-8859-1"),
            headers={"Content-Type": "text/plain"},
            base64=True,
        )

        response = ReturnEventHandler.handler(event, None)

        assert response["body"] == '{"currencySymbol": "£"}'

    def test_can_decode_base64_utf16_body_if_charset_provided(
        self, ReturnEventHandler
    ):
        # This test confirms that we're using the get_encoding_from_headers
        # util from the requests library to parse the charset from content
        # type header, if provided
        event = api_event(
            "post",
            "/",
            body='{"currencySymbol": "£"}'.encode("utf-16"),
            headers={"Content-Type": "unknown/unknown; charset=utf-16"},
            base64=True,
        )

        with patch(
            "infrastructure.handlers.api_gateway.handler.chardet",
            autospec=True,
        ) as chardet:
            response = ReturnEventHandler.handler(event, None)

        assert chardet.detect.call_count == 0

        assert response["body"] == '{"currencySymbol": "£"}'

    def test_can_decode_base64_utf16_body_if_charset_not_provided(
        self, ReturnEventHandler
    ):
        # This test confirms that we have a further fallback for when
        # get_encoding_from_headers doesn't work (chardet) - if we resorted
        # to our default encoding (utf-8) rather than using chardet, this
        # handler would error
        event = api_event(
            "post",
            "/",
            body='{"currencySymbol": "£"}'.encode("utf-16"),
            base64=True,
        )

        response = ReturnEventHandler.handler(event, None)

        assert response["body"] == '{"currencySymbol": "£"}'

    def test_headers_are_case_insensitive(self, PatchedHandler):
        cookie_set_headers = ["some-cookie", "some-other-cookie"]
        event = api_event(
            "post",
            "/",
            headers={"X-Correlation-Id": "some-uuid"},
            multi_value_headers={"Cookie-Set": cookie_set_headers},
        )

        handler = PatchedHandler(event, None)
        handler.parse_event()

        assert handler.headers["x-correlation-id"] == "some-uuid"
        assert handler.headers["cookie-set"] == cookie_set_headers

    def test_create_response_with_all_kwargs(self, ReturnEventWith201Handler):
        body = '{"hello": "world"}'
        headers = {"x-correlation-id": "some-uuid"}
        multi_value_headers = {
            "cookie-set": ["some-cookie", "some-other-cookie"]
        }
        expected_response = {
            "statusCode": 201,
            "body": b64encode(body.encode("utf-8")).decode("utf-8"),
            "headers": headers,
            "multiValueHeaders": multi_value_headers,
            "isBase64Encoded": True,
        }

        event = api_event(
            "post",
            "/",
            body=body,
            headers=headers,
            multi_value_headers=multi_value_headers,
            base64=True,
        )

        response = ReturnEventWith201Handler.handler(event, None)
        assert response == expected_response

    def test_create_response_with_no_kwargs(self, event, PatchedHandler):
        class Handler(PatchedHandler):
            def execute(self):
                return self.create_response()

        assert Handler.handler(event, None) == {"statusCode": 200}

    def test_default_response_without_overriding_execute(
        self, event, PatchedHandler
    ):
        assert PatchedHandler.handler(event, None) == {"statusCode": 200}

    def test_create_response_logs(self, ReturnEventWith201Handler, logger):
        body = '{"hello": "world"}'
        headers = {"x-correlation-id": "some-uuid"}
        multi_value_headers = {
            "cookie-set": ["some-cookie", "some-other-cookie"]
        }
        expected_log = [
            "Returning response",
            {
                "statusCode": 201,
                "body": b64encode(body.encode("utf-8")).decode("utf-8"),
                "headers": headers,
                "multiValueHeaders": multi_value_headers,
                "isBase64Encoded": True,
            },
        ]

        event = api_event(
            "post",
            "/",
            body=body,
            headers=headers,
            multi_value_headers=multi_value_headers,
            base64=True,
        )

        ReturnEventWith201Handler.handler(event, None)

        logger.info.assert_called_with(*expected_log)

    def test_handle_exception_returns_500_by_default(
        self, event, PatchedHandler
    ):
        class Handler(PatchedHandler):
            def execute(self):
                raise KeyError("Nope")

        expected_response = {
            "statusCode": 500,
            "body": "Internal server error",
        }

        assert Handler.handler(event, None) == expected_response

    def test_handle_exception_returns_custom_response(
        self, event, PatchedHandler
    ):
        class HttpTeapot(HttpErrorBase):
            status_code = 418
            default_body = "I'm a teapot"

        class Handler(PatchedHandler):
            def execute(self):
                raise HttpTeapot("Nope")

        expected_response = {
            "statusCode": 418,
            "body": "I'm a teapot",
        }

        assert Handler.handler(event, None) == expected_response

    def test_parsed_body_logged_with_event(self, PatchedHandler, logger):
        class Handler(PatchedHandler):
            def parse_body(self):
                body = super().parse_body()

                return json.loads(body)

        event = api_event(
            "post",
            "/",
            body='{"hello": "world"}'.encode("utf-8"),
            base64=True,
        )

        Handler.handler(event, None)

        body = logger.info.call_args_list[0].args[1]["event"]["body"]

        assert body == {"hello": "world"}
