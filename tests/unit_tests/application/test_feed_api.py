import pytest
from botocore.stub import Stubber
from freezegun import freeze_time

from application.feed_api import handler
from domain.config import NODE, PYTHON
from domain.utils import create_runtime_str
from unit_tests import factories


def dynamodb_item(language, runtime_version, version, prev, time):
    item = {
        "Language": {"S": language},
        "Version": {"S": version},
        "GSI1PK": {"S": "feed"},
        "LSI2SK": {"S": f"{runtime_version}#{time}"},
        "Runtime": {"S": create_runtime_str(language, runtime_version)},
        "Time": {"S": time},
    }

    if prev is not None:
        item["PreviousVersion"] = {"S": prev}

    return item


class TestFeedApiHandler:
    @pytest.fixture(autouse=True)
    def freeze(self):
        with freeze_time("2022-06-26 16:02:36"):
            yield

    @pytest.fixture(autouse=True)
    def dynamodb_stub(self):
        with Stubber(handler.__self__.client) as stubber:
            yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()

    def test_returns_rss_response_with_no_items(self, dynamodb_stub):
        event = factories.feed_api_event()
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss.xml",
            description="Version history of all AWS lambda runtimes",
            timestamp="Sun, 26 Jun 2022 16:02:36 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={"Items": []},
            expected_params={
                "TableName": "test-feed-table",
                "IndexName": "GSI1",
                "KeyConditionExpression": "GSI1PK = :Feed",
                "ExpressionAttributeValues": {":Feed": {"S": "feed"}},
            },
        )

        assert handler(event, None) == expected_response

    def test_returns_rss_response_with_items(self, dynamodb_stub):
        event = factories.feed_api_event()
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss.xml",
            description="Version history of all AWS lambda runtimes",
            items=[
                {
                    "title": "python3.10: 3.10.2",
                    "path": "/rss/python/3.10.xml",
                    "description": (
                        "python3.10 updated: 3.10.1 -&gt; 3.10.2 at "
                        "12PM 25 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Sat, 25 Jun 2022 12:00:00 -0000",
                },
                {
                    "title": "nodejs16.x: 16.1.2",
                    "path": "/rss/node/16.xml",
                    "description": (
                        "nodejs16.x updated: 16.1.1 -&gt; 16.1.2 at "
                        "10AM 25 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Sat, 25 Jun 2022 10:00:00 -0000",
                },
                {
                    "title": "python3.10: 3.10.1",
                    "path": "/rss/python/3.10.xml",
                    "description": (
                        "python3.10 updated: unknown -&gt; 3.10.1 at "
                        "2PM 1 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Wed, 01 Jun 2022 14:00:00 -0000",
                },
            ],
            timestamp="Sat, 25 Jun 2022 12:00:00 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={
                "Items": [
                    dynamodb_item(
                        PYTHON,
                        "3.10",
                        "3.10.2",
                        "3.10.1",
                        "2022-06-25T12:00:00",
                    ),
                    dynamodb_item(
                        NODE, "16", "16.1.2", "16.1.1", "2022-06-25T10:00:00"
                    ),
                    dynamodb_item(
                        PYTHON, "3.10", "3.10.1", None, "2022-06-01T14:00:00"
                    ),
                ]
            },
            expected_params={
                "TableName": "test-feed-table",
                "IndexName": "GSI1",
                "KeyConditionExpression": "GSI1PK = :Feed",
                "ExpressionAttributeValues": {":Feed": {"S": "feed"}},
            },
        )

        assert handler(event, None) == expected_response

    def test_returns_rss_response_with_no_items_for_lang(self, dynamodb_stub):
        event = factories.feed_api_event(language="python")
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss/python.xml",
            description="Version history of AWS lambda Python runtimes",
            timestamp="Sun, 26 Jun 2022 16:02:36 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={"Items": []},
            expected_params={
                "TableName": "test-feed-table",
                "KeyConditionExpression": "#Language = :Language",
                "ExpressionAttributeNames": {"#Language": "Language"},
                "ExpressionAttributeValues": {":Language": {"S": "Python"}},
            },
        )

        assert handler(event, None) == expected_response

    def test_returns_rss_response_with_items_for_lang(self, dynamodb_stub):
        event = factories.feed_api_event(language="python")
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss/python.xml",
            description="Version history of AWS lambda Python runtimes",
            items=[
                {
                    "title": "python3.10: 3.10.2",
                    "path": "/rss/python/3.10.xml",
                    "description": (
                        "python3.10 updated: 3.10.1 -&gt; 3.10.2 at "
                        "12PM 25 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Sat, 25 Jun 2022 12:00:00 -0000",
                },
                {
                    "title": "python3.9: 3.9.1",
                    "path": "/rss/python/3.9.xml",
                    "description": (
                        "python3.9 updated: 3.9.0 -&gt; 3.9.1 at "
                        "4PM 20 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Mon, 20 Jun 2022 16:00:00 -0000",
                },
                {
                    "title": "python3.10: 3.10.1",
                    "path": "/rss/python/3.10.xml",
                    "description": (
                        "python3.10 updated: unknown -&gt; 3.10.1 at "
                        "2PM 1 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Wed, 01 Jun 2022 14:00:00 -0000",
                },
            ],
            timestamp="Sat, 25 Jun 2022 12:00:00 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={
                "Items": [
                    dynamodb_item(
                        PYTHON,
                        "3.10",
                        "3.10.2",
                        "3.10.1",
                        "2022-06-25T12:00:00",
                    ),
                    dynamodb_item(
                        PYTHON, "3.9", "3.9.1", "3.9.0", "2022-06-20T16:00:00"
                    ),
                    dynamodb_item(
                        PYTHON, "3.10", "3.10.1", None, "2022-06-01T14:00:00"
                    ),
                ]
            },
            expected_params={
                "TableName": "test-feed-table",
                "KeyConditionExpression": "#Language = :Language",
                "ExpressionAttributeNames": {"#Language": "Language"},
                "ExpressionAttributeValues": {":Language": {"S": "Python"}},
            },
        )

        assert handler(event, None) == expected_response

    def test_returns_rss_response_with_no_items_for_version(
        self, dynamodb_stub
    ):
        event = factories.feed_api_event(language="python", version="3.12")
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss/python/3.12.xml",
            description="Version history of AWS lambda Python 3.12 runtimes",
            timestamp="Sun, 26 Jun 2022 16:02:36 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={"Items": []},
            expected_params={
                "TableName": "test-feed-table",
                "KeyConditionExpression": (
                    "#Language = :Language AND begins_with(#Version, :Version)"
                ),
                "ExpressionAttributeNames": {
                    "#Language": "Language",
                    "#Version": "Version",
                },
                "ExpressionAttributeValues": {
                    ":Language": {"S": "Python"},
                    ":Version": {"S": "3.12"},
                },
            },
        )

        assert handler(event, None) == expected_response

    def test_returns_rss_response_with_items_for_version(self, dynamodb_stub):
        event = factories.feed_api_event(language="python", version="3.12")
        expected_body = factories.rss_feed(
            title="AWS lambda runtime versions",
            path="/rss/python/3.12.xml",
            description="Version history of AWS lambda Python 3.12 runtimes",
            items=[
                {
                    "title": "python3.12: 3.12.2",
                    "path": "/rss/python/3.12.xml",
                    "description": (
                        "python3.12 updated: 3.12.1 -&gt; 3.12.2 at "
                        "12PM 25 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Sat, 25 Jun 2022 12:00:00 -0000",
                },
                {
                    "title": "python3.12: 3.12.1",
                    "path": "/rss/python/3.12.xml",
                    "description": (
                        "python3.12 updated: unknown -&gt; 3.12.1 at "
                        "4PM 20 Jun 2022 (UTC)"
                    ),
                    "timestamp": "Mon, 20 Jun 2022 16:00:00 -0000",
                },
            ],
            timestamp="Sat, 25 Jun 2022 12:00:00 -0000",
        )
        expected_response = {
            "statusCode": 200,
            "body": expected_body,
            "headers": {"Content-Type": "text/xml; charset=utf-8"},
        }
        dynamodb_stub.add_response(
            "query",
            service_response={
                "Items": [
                    dynamodb_item(
                        PYTHON,
                        "3.12",
                        "3.12.2",
                        "3.12.1",
                        "2022-06-25T12:00:00",
                    ),
                    dynamodb_item(
                        PYTHON, "3.12", "3.12.1", None, "2022-06-20T16:00:00"
                    ),
                ]
            },
            expected_params={
                "TableName": "test-feed-table",
                "KeyConditionExpression": (
                    "#Language = :Language AND begins_with(#Version, :Version)"
                ),
                "ExpressionAttributeNames": {
                    "#Language": "Language",
                    "#Version": "Version",
                },
                "ExpressionAttributeValues": {
                    ":Language": {"S": "Python"},
                    ":Version": {"S": "3.12"},
                },
            },
        )

        assert handler(event, None) == expected_response

    @pytest.mark.parametrize(
        "method",
        [
            pytest.param("HEAD", id="head"),
            pytest.param("POST", id="post"),
            pytest.param("PUT", id="put"),
            pytest.param("PATCH", id="patch"),
            pytest.param("OPTIONS", id="options"),
            pytest.param("DELETE", id="delete"),
        ],
    )
    def test_returns_404_for_invalid_method(self, method):
        event = factories.feed_api_event(method=method, path="/rss.xml")
        expected_response = {
            "statusCode": 404,
            "body": "Not Found",
        }

        assert handler(event, None) == expected_response

    def test_returns_404_for_invalid_path(self):
        event = factories.feed_api_event(path="/rss//3.12.xml")
        expected_response = {
            "statusCode": 404,
            "body": "Invalid path requested: /rss//3.12.xml",
        }

        assert handler(event, None) == expected_response

    def test_returns_404_for_invalid_lang(self):
        event = factories.feed_api_event(language="ruby")
        expected_response = {
            "statusCode": 404,
            "body": "Invalid runtime language: Ruby",
        }

        assert handler(event, None) == expected_response

    def test_returns_404_for_unknown_version(self):
        event = factories.feed_api_event(language="python", version="3.8")
        expected_response = {
            "statusCode": 404,
            "body": "Invalid runtime version: Python 3.8",
        }

        assert handler(event, None) == expected_response
