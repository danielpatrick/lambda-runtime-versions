from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber
from freezegun import freeze_time

from application.version_stream import handler
from infrastructure.logger import Logger
from unit_tests.factories import version_stream_record


class TestVersionStreamHandler:
    @pytest.fixture
    def logger(self):
        mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

        handler.__self__.create_logger = lambda self: mock_logger

        yield mock_logger

    @pytest.fixture(autouse=True)
    def freeze(self):
        with freeze_time("2022-06-26 16:02:36"):
            yield

    @pytest.fixture(autouse=True)
    def dynamodb_stub(self):
        with Stubber(handler.__self__.client) as stubber:
            yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()

    def test_writes_to_feed_table(self, dynamodb_stub):
        record = version_stream_record(
            runtime="python3.9",
            language="Python",
            runtime_version="3.9",
            old_version="3.9.1",
            new_version="3.9.2",
        )

        dynamodb_stub.add_response(
            "put_item",
            service_response={},
            expected_params={
                "TableName": "test-feed-table",
                "Item": {
                    "Language": {"S": "Python"},
                    "Version": {"S": "3.9.2"},
                    "LSI2SK": {"S": "3.9#2022-06-26T16:00:00"},
                    "Time": {"S": "2022-06-26T16:00:00"},
                    "Runtime": {"S": "python3.9"},
                    "PreviousVersion": {"S": "3.9.1"},
                    "GSI1PK": {"S": "feed"},
                },
                "ConditionExpression": "attribute_not_exists(Version)",
            },
        )

        handler({"Records": [record]}, None)

    def test_writes_without_old_version_on_insert(self, dynamodb_stub, logger):
        record = version_stream_record(
            runtime="python3.9",
            language="Python",
            runtime_version="3.9",
            new_version="3.9.2",
            event_name="INSERT",
        )

        dynamodb_stub.add_response(
            "put_item",
            service_response={},
            expected_params={
                "TableName": "test-feed-table",
                "Item": {
                    "Language": {"S": "Python"},
                    "Version": {"S": "3.9.2"},
                    "LSI2SK": {"S": "3.9#2022-06-26T16:00:00"},
                    "Time": {"S": "2022-06-26T16:00:00"},
                    "Runtime": {"S": "python3.9"},
                    "GSI1PK": {"S": "feed"},
                },
                "ConditionExpression": "attribute_not_exists(Version)",
            },
        )

        handler({"Records": [record]}, None)

    def test_handles_condition_expression_failure(self, dynamodb_stub, logger):
        record = version_stream_record(
            runtime="python3.9",
            language="Python",
            runtime_version="3.9",
            new_version="3.9.2",
            event_name="INSERT",
        )

        dynamodb_stub.add_client_error(
            "put_item",
            service_error_code="ConditionalCheckFailedException",
            http_status_code=400,
            expected_params={
                "TableName": "test-feed-table",
                "Item": {
                    "Language": {"S": "Python"},
                    "Version": {"S": "3.9.2"},
                    "LSI2SK": {"S": "3.9#2022-06-26T16:00:00"},
                    "Time": {"S": "2022-06-26T16:00:00"},
                    "Runtime": {"S": "python3.9"},
                    "GSI1PK": {"S": "feed"},
                },
                "ConditionExpression": "attribute_not_exists(Version)",
            },
        )

        handler({"Records": [record]}, None)
        logger.info.assert_any_call(
            "Record not written: version already exists",
            {"record": record},
        )

    def test_does_not_write_removals(self, dynamodb_stub, logger):
        record = version_stream_record(
            runtime="python3.9",
            language="Python",
            runtime_version="3.9",
            old_version="3.9.1",
            event_name="REMOVE",
        )

        handler({"Records": [record]}, None)
        logger.info.assert_any_call("Skipping record", {"record": record})
