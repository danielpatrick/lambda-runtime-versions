from unittest.mock import MagicMock

import pytest
from botocore.stub import Stubber

from application.update_versions import handler
from infrastructure.logger import Logger
from unit_tests.factories import scheduled_event

items = [
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "python3.9"},
                "Language": {"S": "Python"},
                "RuntimeVersion": {"S": "3.9"},
                "Version": {"S": "3.9.19"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "python3.10"},
                "Language": {"S": "Python"},
                "RuntimeVersion": {"S": "3.10"},
                "Version": {"S": "3.10.14"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "python3.11"},
                "Language": {"S": "Python"},
                "RuntimeVersion": {"S": "3.11"},
                "Version": {"S": "3.11.9"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "python3.12"},
                "Language": {"S": "Python"},
                "RuntimeVersion": {"S": "3.12"},
                "Version": {"S": "3.12.3"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "nodejs16.x"},
                "Language": {"S": "Node"},
                "RuntimeVersion": {"S": "16"},
                "Version": {"S": "16.20.2"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "nodejs18.x"},
                "Language": {"S": "Node"},
                "RuntimeVersion": {"S": "18"},
                "Version": {"S": "18.20.0"},
            }
        }
    },
    {
        "PutRequest": {
            "Item": {
                "Runtime": {"S": "nodejs20.x"},
                "Language": {"S": "Node"},
                "RuntimeVersion": {"S": "20"},
                "Version": {"S": "20.12.0"},
            }
        }
    },
]


class TestUpdateVersionsHandler:
    @pytest.fixture
    def logger(self):
        mock_logger = MagicMock(wraps=Logger("SERVICE", "SOURCE"))

        handler.__self__.create_logger = lambda self: mock_logger

        yield mock_logger

    @pytest.fixture(autouse=True)
    def mock_api(self, requests_mock):
        requests_mock.get("https://example.com/python/3.9", text="3.9.19")
        requests_mock.get("https://example.com/python/3.10", text="3.10.14")
        requests_mock.get("https://example.com/python/3.11", text="3.11.9")
        requests_mock.get("https://example.com/python/3.12", text="3.12.3")
        requests_mock.get("https://example.com/node/16", text="16.20.2")
        requests_mock.get("https://example.com/node/18", text="18.20.0")
        requests_mock.get("https://example.com/node/20", text="20.12.0")

    @pytest.fixture(autouse=True)
    def dynamodb_stub(self):
        with Stubber(handler.__self__.client) as stubber:
            yield stubber

        # Confirm tests made all expected requests
        stubber.assert_no_pending_responses()

    def test_calls_dynamodb_batch(self, dynamodb_stub):
        dynamodb_stub.add_response(
            "batch_write_item",
            service_response={},
            expected_params={"RequestItems": {"test-latest-table": items}},
        )

        handler(scheduled_event(), None)

    def test_calls_dynamodb_batch_with_one_failure(
        self, dynamodb_stub, requests_mock, logger
    ):
        requests_mock.get(
            "https://example.com/python/3.11",
            text="oh no",
            status_code=500,
        )
        dynamodb_stub.add_response(
            "batch_write_item",
            service_response={},
            expected_params={
                "RequestItems": {"test-latest-table": items[:2] + items[3:]}
            },
        )

        handler(scheduled_event(), None)

        logger.error.assert_any_call(
            "Unable to fetch version info",
            {
                "url": "https://example.com/python/3.11",
                "method": "GET",
                "response_body": "oh no",
                "status": 500,
            },
        )
