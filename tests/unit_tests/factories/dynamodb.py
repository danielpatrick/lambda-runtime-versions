import os
import time
from uuid import uuid4


def dynamodb_stream_record(
    *,
    keys,
    event_name,
    stream_view_type,
    old_image,
    new_image,
):
    region = os.environ["AWS_DEFAULT_REGION"]

    record = {
        "eventID": str(uuid4()),
        "eventName": event_name,
        "eventVersion": "1.1",
        "eventSource": "aws:dynamodb",
        "awsRegion": region,
        "dynamodb": {
            "ApproximateCreationDateTime": str(int(time.time())),
            "Keys": keys,
            "SequenceNumber": "123",
            "SizeBytes": 123,
            "StreamViewType": stream_view_type,
        },
        "eventSourceArn": "stream-arn",
    }

    if event_name in ("REMOVE", "MODIFY") and stream_view_type in (
        "OLD_IMAGE",
        "NEW_AND_OLD_IMAGES",
    ):
        record["dynamodb"]["OldImage"] = old_image
    if event_name in ("INSERT", "MODIFY") and stream_view_type in (
        "NEW_AND_OLD_IMAGES",
        "NEW_IMAGE",
    ):
        record["dynamodb"]["NewImage"] = new_image

    return record


def version_dynamodb_image(
    runtime,
    language,
    runtime_version,
    language_version,
):
    return {
        "Runtime": {"S": runtime},
        "Language": {"S": language},
        "RuntimeVersion": {"S": runtime_version},
        "Version": {"S": language_version},
    }


def version_stream_record(
    *,
    runtime="python3.9",
    language="Python",
    runtime_version="3.9",
    old_version="3.9.1",
    new_version="3.9.2",
    event_name="MODIFY",
    stream_view_type="NEW_AND_OLD_IMAGES",
):
    old_image = version_dynamodb_image(
        runtime, language, runtime_version, old_version
    )
    new_image = version_dynamodb_image(
        runtime, language, runtime_version, new_version
    )

    keys = {"Runtime": new_image["Runtime"]}

    return dynamodb_stream_record(
        keys=keys,
        event_name=event_name,
        stream_view_type=stream_view_type,
        old_image=old_image,
        new_image=new_image,
    )
