from base64 import b64encode


def api_event(method, path, **kwargs):
    base64 = kwargs.get("base64", False)
    body = kwargs.get("body", "")
    path_parameters = kwargs.get("path_parameters", {})

    if base64:
        if isinstance(body, str):
            body = body.encode("utf-8")

        body = b64encode(body).decode("utf-8")

    event = {
        "headers": kwargs.get("headers", {}),
        "multiValueHeaders": kwargs.get("multi_value_headers", {}),
        "requestContext": {
            "domainName": "www.example.com",
            "http": {
                "method": method,
                "path": path,
            },
        },
        "body": body,
        "isBase64Encoded": base64,
    }

    if path_parameters is not None:
        event["pathParameters"] = path_parameters

    return event


def feed_api_event(*, language=None, version=None, **kwargs):
    path_parameters = kwargs.get("path_parameters", {})
    method = kwargs.pop("method", "GET")

    _path = "/rss"

    if language is not None:
        _path += f"/{language}"

        if "language" not in path_parameters:
            path_parameters["language"] = language

    if version is not None:
        _path += f"/{version}"

        if "version" not in path_parameters:
            path_parameters["version"] = version

    _path += ".xml"

    path = kwargs.pop("path", _path)

    return api_event(method, path, path_parameters=path_parameters, **kwargs)
