def rss_feed_item(*, title, path, description, timestamp):
    return (
        "<item>"
        f"<title>{title}</title>"
        f"<link>https://www.example.com{path}</link>"
        f"<description>{description}</description>"
        f"<pubDate>{timestamp}</pubDate>"
        "</item>"
    )


def rss_feed(*, title, path, description, timestamp, items=None):
    if items is None:
        items = []

    result = (
        '<?xml version="1.0" encoding="utf-8"?>\n<rss version="2.0"><channel>'
        f"<title>{title}</title>"
        f"<link>https://www.example.com{path}</link>"
        f"<description>{description}</description>"
        f"<lastBuildDate>{timestamp}</lastBuildDate>"
    )

    result += "".join(rss_feed_item(**item) for item in items)
    result += "</channel></rss>"

    return result
