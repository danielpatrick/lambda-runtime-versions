import os

import boto3
import pytest
import requests
from rss_parser import RSSParser

LATEST_TABLE_NAME = os.environ["TEST_FEED_TABLE_NAME"]
FEED_TABLE_NAME = os.environ["TEST_LATEST_TABLE_NAME"]
DOMAIN_NAME = os.environ["TEST_DOMAIN_NAME"]
client = boto3.client("dynamodb")


def list_items():
    kwargs = {
        "TableName": LATEST_TABLE_NAME,
        "PaginationConfig": {},
    }
    paginator = client.get_paginator("scan")

    for page in paginator.paginate(**kwargs):
        for item in page.get("Items", []):
            yield item


@pytest.fixture(scope="session", autouse=True)
def empty_table():
    for item in list_items():
        client.delete_item(
            TableName=LATEST_TABLE_NAME,
            Key={
                "Language": item["Language"],
                "Version": item["Version"],
            },
        )


PARAMS = [
    pytest.param(
        "/rss.xml",
        "Version history of all AWS lambda runtimes",
        id="main",
    ),
    pytest.param(
        "/rss/node.xml",
        "Version history of AWS lambda Node runtimes",
        id="node",
    ),
    pytest.param(
        "/rss/node/16.xml",
        "Version history of AWS lambda Node 16 runtimes",
        id="node_16",
    ),
    pytest.param(
        "/rss/node/18.xml",
        "Version history of AWS lambda Node 18 runtimes",
        id="node_18",
    ),
    pytest.param(
        "/rss/node/20.xml",
        "Version history of AWS lambda Node 20 runtimes",
        id="node_20",
    ),
    pytest.param(
        "/rss/python.xml",
        "Version history of AWS lambda Python runtimes",
        id="python",
    ),
    pytest.param(
        "/rss/python/3.9.xml",
        "Version history of AWS lambda Python 3.9 runtimes",
        id="python_3.9",
    ),
    pytest.param(
        "/rss/python/3.10.xml",
        "Version history of AWS lambda Python 3.10 runtimes",
        id="python_3.10",
    ),
    pytest.param(
        "/rss/python/3.11.xml",
        "Version history of AWS lambda Python 3.11 runtimes",
        id="python_3.11",
    ),
    pytest.param(
        "/rss/python/3.12.xml",
        "Version history of AWS lambda Python 3.12 runtimes",
        id="python_3.12",
    ),
]


@pytest.mark.parametrize("path,expected_description", PARAMS)
def test_empty_rss_feed(path, expected_description):
    expected_content_type = "text/xml; charset=utf-8"
    response = requests.get(f"https://{DOMAIN_NAME}{path}")

    assert response.status_code == 200
    assert response.headers["Content-Type"] == expected_content_type

    feed = RSSParser.parse(response.text)

    assert feed.version.content == "2.0"
    assert feed.channel.title.content == "AWS lambda runtime versions"
    assert feed.channel.description.content == expected_description
    assert feed.channel.items == []


UNKNOWN_PATHS = [
    pytest.param(
        "/rsss.xml",
        "Invalid path requested: /rsss.xml",
        id="typo",
    ),
    pytest.param(
        "/rss/ruby.xml",
        "Invalid runtime language: Ruby",
        id="ruby",
    ),
    pytest.param(
        "/rss/ruby/3.3.xml",
        "Invalid runtime language: Ruby",
        id="ruby_3.3",
    ),
    pytest.param(
        "/rss/python/3.8.xml",
        "Invalid runtime version: Python 3.8",
        id="python_3.8",
    ),
]


@pytest.mark.parametrize("path,expected_body", UNKNOWN_PATHS)
def test_invalid_paths_rss_feed(path, expected_body):
    expected_content_type = "text/plain; charset=utf-8"
    response = requests.get(f"https://{DOMAIN_NAME}{path}")

    assert response.status_code == 404
    assert response.headers["Content-Type"] == expected_content_type
    assert response.text == expected_body
