.PHONY: install local format lint cfn-lint flake8 black black-fix isort isort-fix bandit safety test build FORCE

FORCE:

install:
	poetry install

update:
	poetry update

local: install
	poetry run pre-commit install

lint: flake8 black cfn-lint isort

format: black-fix isort-fix

flake8:
	poetry run flake8

black:
	poetry run black --diff --check .

black-fix:
	poetry run black .

isort:
	poetry run isort --check --diff .

isort-fix:
	poetry run isort .

cfn-lint:
	poetry run cfn-lint template.yaml

bandit:
	poetry run bandit -r lambda_runtime_versions -q -n 3

safety:
	poetry export -f requirements.txt | poetry run safety check --stdin

utest:
	poetry run pytest \
		--cov-report term:skip-covered \
		--cov-report html:reports \
		--junitxml=reports/unit_test_coverage.xml \
		--cov=lambda_runtime_versions \
		--cov-fail-under=100 \
		tests/unit_tests -ra -s

itest:
	poetry run pytest \
		tests/integration_tests -ra -s

test: utest

.DELETE_ON_ERROR:
requirements.txt:
	poetry export -f requirements.txt -o requirements.txt
	export SAM_CLI_TELEMETRY=0 && \
		sam build \
		-m requirements.txt \
		-t template.yaml \
		--debug \
		--exclude Python39 \
		--exclude Python310 \
		--exclude Python311 \
		--exclude Python312 \
		--exclude Node16 \
		--exclude Node18 \
		--exclude Node20
	rm requirements.txt

build: requirements.txt
