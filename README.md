# lambda-runtime-versions

APIs for finding out the current versions of lambda runtimes

## Usage

```shell
$ curl -w "\n" https://your.domain.com/node/20
20.12.0

$ curl -w "\n" https://your.domain.com/python/3.12
3.12.3
```

## Contributing

### Updating runtime versions

1. Update the [list of versions in code](./lambda_runtime_versions/domain/config.py#L8-9)
2. Update tests to match
2. Add/remove version check lambda functions from the [SAM template](./template.yaml#L177) as required
3. Update the functions excluded from the SAM build in the [Makefile](./Makefile#L66)
4. Bump the `cfn-lint` dev dependency if required (using `poetry`), as it rejects runtime version it doesn't know about
