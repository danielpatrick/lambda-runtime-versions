"""Common config."""

DATETIME_FORMAT = "%Y-%m-%dT%H:00:00"
TIME_PRINT_FORMAT = "%-I%p %-d %b %Y (UTC)"

NODE = "Node"
PYTHON = "Python"
RUNTIMES = {
    PYTHON: ["3.9", "3.10", "3.11", "3.12"],
    NODE: ["16", "18", "20"],
}
