"""Misc utilities."""

from .config import NODE, PYTHON

RUNTIME_STR = {
    NODE: "nodejs{version}.x",
    PYTHON: "python{version}",
}


def create_runtime_str(language, version):
    """Create string representation of runtime version."""
    return RUNTIME_STR[language].format(version=version)
