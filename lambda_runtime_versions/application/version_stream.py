"""Update feed table from dynamodb stream."""

import os
from datetime import datetime

import boto3

from domain.config import DATETIME_FORMAT
from infrastructure.handlers.base import BaseHandler

TABLE_NAME = os.environ["FEED_TABLE_NAME"]


class VersionStreamHandler(BaseHandler):
    """Update feed table from dynamodb stream."""

    client = boto3.client("dynamodb")

    def write_to_dynamodb(self, record):
        """Put item in dynamodb."""
        new_image = record["dynamodb"]["NewImage"]
        version = new_image["Version"]["S"]

        runtime = new_image["Runtime"]["S"]
        language = new_image["Language"]["S"]
        runtime_version = new_image["RuntimeVersion"]["S"]

        time = datetime.utcnow().strftime(DATETIME_FORMAT)

        item = {
            "Language": {"S": language},
            "Version": {"S": version},
            "LSI2SK": {"S": f"{runtime_version}#{time}"},
            "Time": {"S": time},
            "Runtime": {"S": runtime},
            "GSI1PK": {"S": "feed"},
        }

        if "OldImage" in record["dynamodb"]:
            old_image = record["dynamodb"]["OldImage"]
            item["PreviousVersion"] = {"S": old_image["Version"]["S"]}

        try:
            self.client.put_item(
                TableName=TABLE_NAME,
                Item=item,
                ConditionExpression="attribute_not_exists(Version)",
            )
        except self.client.exceptions.ConditionalCheckFailedException:
            self.logger.info(
                "Record not written: version already exists",
                {"record": record},
            )

    def execute(self):
        """Do the thing."""
        for record in self.event["Records"]:
            if record["eventName"] in ("MODIFY", "INSERT"):
                self.write_to_dynamodb(record)
            else:
                self.logger.info(
                    "Skipping record",
                    {"record": record},
                )


handler = VersionStreamHandler.handler
