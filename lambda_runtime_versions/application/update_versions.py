"""Check the latest versions on a schedule and update dynamodb."""

import os

import boto3
import requests

from domain.config import RUNTIMES
from domain.utils import create_runtime_str
from infrastructure.handlers.base import BaseHandler

DOMAIN_NAME = os.environ["DOMAIN_NAME"]
TABLE_NAME = os.environ["LATEST_TABLE_NAME"]


class UpdateVersionsHandler(BaseHandler):
    """Check the latest versions on a schedule and update dynamodb."""

    client = boto3.client("dynamodb")

    def put_item_params(self, language, runtime_version, language_version):
        """Create params for put item request."""
        return {
            "PutRequest": {
                "Item": {
                    "Runtime": {
                        "S": create_runtime_str(language, runtime_version)
                    },
                    "Language": {"S": language},
                    "RuntimeVersion": {"S": runtime_version},
                    "Version": {"S": language_version},
                },
            }
        }

    def execute(self):
        """Do the thing."""
        write_items = []

        for language in RUNTIMES:
            for runtime_version in RUNTIMES[language]:
                url = (
                    "https://"
                    f"{DOMAIN_NAME}/{language.lower()}/{runtime_version}"
                )
                r = requests.get(url, timeout=30)

                if r.status_code == 200:
                    write_items.append(
                        self.put_item_params(
                            language,
                            runtime_version,
                            r.text,
                        )
                    )
                else:
                    self.logger.error(
                        "Unable to fetch version info",
                        {
                            "url": url,
                            "method": "GET",
                            "response_body": r.text,
                            "status": r.status_code,
                        },
                    )

        self.client.batch_write_item(RequestItems={TABLE_NAME: write_items})


handler = UpdateVersionsHandler.handler
