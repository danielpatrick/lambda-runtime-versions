"""Handle /feed API requests."""

import os
import re
from datetime import datetime

import boto3
from feedgenerator import Rss201rev2Feed

from domain.config import DATETIME_FORMAT, RUNTIMES, TIME_PRINT_FORMAT
from infrastructure.handlers.api_gateway import ApiGatewayV2Handler
from infrastructure.handlers.api_gateway.exceptions import HttpNotFoundError

TABLE_NAME = os.environ["FEED_TABLE_NAME"]

REGEX = re.compile(
    r"^/rss"
    r"(?:/(?P<language>[^/]+)"
    r"(?:/(?P<version>[0-9.]+))?)?"
    r"\.xml$"
)


class FeedApiHandler(ApiGatewayV2Handler):
    """Slack callback redirected to after user accepts scopes on Slack site."""

    client = boto3.client("dynamodb")

    def full_kwargs(self):
        """Kwargs for query for all items in dynamodb."""
        return {
            "TableName": TABLE_NAME,
            "IndexName": "GSI1",
            "KeyConditionExpression": "GSI1PK = :Feed",
            "ExpressionAttributeValues": {":Feed": {"S": "feed"}},
            "PaginationConfig": {},
        }

    def language_kwargs(self, language):
        """Kwargs for query for a given language."""
        return {
            "TableName": TABLE_NAME,
            "KeyConditionExpression": "#Language = :Language",
            "ExpressionAttributeValues": {
                ":Language": {"S": language.capitalize()}
            },
            "ExpressionAttributeNames": {
                "#Language": "Language",
            },
            "PaginationConfig": {},
        }

    def runtime_kwargs(self, language, version):
        """Kwargs for query for a given language runtime."""
        return {
            "TableName": TABLE_NAME,
            "KeyConditionExpression": (
                "#Language = :Language AND begins_with(#Version, :Version)"
            ),
            "ExpressionAttributeNames": {
                "#Language": "Language",
                "#Version": "Version",
            },
            "ExpressionAttributeValues": {
                ":Language": {"S": language.capitalize()},
                ":Version": {"S": version},
            },
            "PaginationConfig": {},
        }

    def create_kwargs(self, language, version):
        """Create dynamodb query arguments."""
        if language is None:
            return self.full_kwargs()
        elif language not in RUNTIMES:
            message = f"Invalid runtime language: {language}"

            raise HttpNotFoundError(message, body=message)
        elif version is None:
            return self.language_kwargs(language)
        elif version not in RUNTIMES[language]:
            message = f"Invalid runtime version: {language} {version}"

            raise HttpNotFoundError(message, body=message)

        return self.runtime_kwargs(language, version)

    def fetch_items(self, language, version):
        """Fetch items from dynamodb, handling pagination."""
        query_kwargs = self.create_kwargs(language, version)
        paginator = self.client.get_paginator("query")

        for page in paginator.paginate(**query_kwargs):
            for item in page.get("Items", []):
                yield item

    def create_description(self, language, version):
        """Create RSS feed description."""
        if language is None:
            return "Version history of all AWS lambda runtimes"
        elif version is None:
            return f"Version history of AWS lambda {language} runtimes"

        return f"Version history of AWS lambda {language} {version} runtimes"

    def execute(self):
        """Create RSS feed HTTP response."""
        method = self.event["requestContext"]["http"]["method"]

        if method.lower() != "get":
            raise HttpNotFoundError(f"HTTP method not allowed: {method}")

        title = "AWS lambda runtime versions"

        domain = self.event["requestContext"]["domainName"]
        path = self.event["requestContext"]["http"]["path"]
        url = f"https://{domain}{path}"

        match = REGEX.match(path)

        if match is None:
            message = f"Invalid path requested: {path}"
            raise HttpNotFoundError(message, body=message)

        language, version = match.groups()
        language = language.capitalize() if language is not None else None

        description = self.create_description(language, version)

        feed = Rss201rev2Feed(title, url, description)

        for item in self.fetch_items(language, version):
            old_version = "unknown"

            if "PreviousVersion" in item:
                old_version = item["PreviousVersion"]["S"]

            new_version = item["Version"]["S"]
            runtime = item["Runtime"]["S"]
            time = datetime.strptime(item["Time"]["S"], DATETIME_FORMAT)
            timestr = time.strftime(TIME_PRINT_FORMAT)

            local_language = item["Language"]["S"].lower()
            local_version = item["LSI2SK"]["S"].split("#", 1)[0]
            local_url = (
                f"https://{domain}/rss/"
                f"{local_language}/{local_version}.xml"
            )

            feed.add_item(
                f"{runtime}: {new_version}",
                local_url,
                (
                    f"{runtime} updated: "
                    f"{old_version} -> {new_version}"
                    f" at {timestr}"
                ),
                pubdate=time,
            )

        return self.create_response(
            body=feed.writeString("utf-8"),
            headers={"Content-Type": "text/xml; charset=utf-8"},
        )


handler = FeedApiHandler.handler
