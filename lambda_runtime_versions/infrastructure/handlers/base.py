"""Base lambda handler providing a framework for handling lambda events."""

import os

from infrastructure.logger import Logger


class BaseHandler:
    """Handle lambda events."""

    SERVICE = os.environ.get("SERVICE_NAME", "unknown")
    SOURCE = os.environ.get("SOURCE_NAME", "unknown")
    LOG_REDACTION_KEYS = set()
    FAIL_ON_ERROR = True

    def __init__(self, event, context):
        """
        Class for processing a lambda event.

        Not instantiated directly - use `handler` method.

        Parameters:
            event: lambda event object
            context: lambda context object
        """
        self.event = event
        self.context = context
        self.logger = self.create_logger()

    def create_logger(self):
        """Create a logger instance for this handler."""
        return Logger(self.SERVICE, self.SOURCE, self.LOG_REDACTION_KEYS)

    def parse_event(self):
        """
        Parse the event.

        This is a good place to set any state that might be useful for both
        logging and handling the event.
        """
        pass

    def loggable_event(self):
        """
        Return a potentially modified version of the event object.

        Event may be modified for better logging properties.
        """
        return self.event

    def set_logger_attrs(self):
        """Set any invocation-specific logger state."""
        pass

    @classmethod
    def handler(cls, event, context):
        """Orchestrate the eveny handling process."""
        instance = cls(event, context)

        parse_event_error = None

        try:
            instance.parse_event()
        except Exception as e:
            parse_event_error = e

        if parse_event_error is None:
            try:
                instance.set_logger_attrs()
            except Exception as e:
                instance.logger.warning(
                    f"{instance.__class__.__name__}.set_logger_attrs failed",
                    exception=e,
                )

        if parse_event_error is not None:
            instance.logger.info(
                "Lambda invocation started",
                {"event": instance.event},
            )

            instance.logger.error(
                f"{instance.__class__.__name__}.parse_event failed"
            )

            return instance.handle_exception(parse_event_error)

        instance.logger.info(
            "Lambda invocation started",
            {"event": instance.loggable_event()},
        )

        try:
            return instance.execute()
        except Exception as e:
            instance.logger.error(
                f"{instance.__class__.__name__}.execute failed"
            )
            return instance.handle_exception(e)

    def execute(self):
        """
        Handle the event as required.

        The response will be used as the lambda response.
        """
        pass

    def handle_exception(self, exception):
        """
        Deal with any exception that occurs.

        The response from this will be used as the lambda response.

        Default behaviour is to log and re-throw, which makes sense for
        single-batch event-driven lambdas, but not for more complex use cases.
        """
        self.logger.error("Lambda invocation failure", exception=exception)

        if self.FAIL_ON_ERROR:
            raise exception
